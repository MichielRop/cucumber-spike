# features/table.feature
Feature: Users

  Scenario: Creating Users
    Given the following users:
      | user          | followers |
      | aslakhellesoy | 317       |
      | jberkel       | 51        |
      | bfaloona      | 4         |
      | mattwynne     | 33        |
    Then the total number of users is 4
    And the maximum number of followers is 317
    And the user with the most followers is "aslakhellesoy"