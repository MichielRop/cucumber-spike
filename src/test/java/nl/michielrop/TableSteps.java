package nl.michielrop;

import java.util.ArrayList;
import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;
import static org.junit.Assert.assertEquals;

public class TableSteps {


    private List<User> actualUsers =  new ArrayList<User>();;
    private UserList userList;

    @Given("^the following users:$")
    public void the_following_users(DataTable dataTable) throws Throwable {
        actualUsers = dataTable.asList(User.class);
        userList = new UserList(actualUsers);

    }

    @Then("^the total number of users is (\\d+)$")
    public void the_total_number_of_users_is(int number) throws Throwable {
        Assert.assertEquals(userList.getTotalNumberOfUsers(),number);

    }

    @And("^the user with the most followers is \"([^\"]*)\"$")
    public void the_user_with_the_most_followers_is(String user) throws Throwable {
        Assert.assertEquals(userList.getUserWithMostFollowers().user,user);

    }


    @And("^the maximum number of followers is (\\d+)$")
    public void the_maximum_number_of_followers_is(int arg1) throws Throwable {
       assertEquals(userList.getUserWithMostFollowers().followers,arg1);
    }
}


