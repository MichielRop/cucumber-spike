package nl.michielrop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserList {

    private List<User> userList = new ArrayList<User>();

    public UserList(List<User> userList) {
        this.userList = userList;
    }

    public int getTotalNumberOfUsers(){
        return userList.size();
    }

    public void add(User user){
        userList.add(user);
    }

    public User getUserWithMostFollowers(){
        List<User> list = getUserList();
        Collections.sort(list);
        if (list.size()>0){
            return  list.get(0);
        }
        else{
            return null;
        }
    }


    public List<User> getUserList(){
        List<User> result = new ArrayList<User>();
        result.addAll(userList);
        return result;
    }


}
