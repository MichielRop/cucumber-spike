package nl.michielrop;

public class User implements Comparable<User> {

    String user;
    int followers;

    public User(String user,int followers){
        this.user = user;
        this.followers = followers;
    }

    @Override
    public String toString(){
        return user + "," + followers;
    }

    public int getFollowers() {
        return followers;
    }


    @Override
    public int compareTo(final User o) {
        return new Integer(o.getFollowers()).compareTo(getFollowers());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }

        final User user1 = (User) o;

        if (followers != user1.followers) {
            return false;
        }
        if (user != null ? !user.equals(user1.user) : user1.user != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + followers;
        return result;
    }
}
